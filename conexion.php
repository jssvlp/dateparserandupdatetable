<?php 

require_once('./vendor/autoload.php');

class Conexion{

	public $conn;

	public function __construct(){

		$dotenv = Dotenv\Dotenv::create(__DIR__);
		$dotenv->load();

		$serverName = getenv('HOST');

		$connectionInfo = array(
			"Database"=>getenv('DATABASE'),
			"UID"=>getenv('DBUSERNAME'),
			"PWD"=>getenv('PASSWORD'),
		);

		$this->conn = sqlsrv_connect($serverName, $connectionInfo);

		if($this->conn) {
		     return $this->conn;
		}else{
		     echo "Problemas tecnicos.<br />";
		     die( print_r( sqlsrv_errors(), true));
		}

	}

	public static function getMont($fecha){
		$mont = intval(date("m", strtotime($fecha)));
		switch ($mont) {
			case 1:$mont_name = 'Enero';break;
			case 2:$mont_name = 'Febrero';break;
			case 3:$mont_name = 'Marzo';break;
			case 4:$mont_name = 'Abril';break;
			case 5:$mont_name = 'Mayo';break;
			case 6:$mont_name = 'Junio';break;
			case 7:$mont_name = 'Julio';break;
			case 8:$mont_name = 'Agosto';break;
			case 9:$mont_name = 'Septiembre';break;
			case 10:$mont_name = 'Octubre';break;
			case 11:$mont_name = 'Noviembre';break;
			case 12:$mont_name = 'Diciembre';break;
			default:$mont_name = 'Ningun mes';break;
		}
		return $mont_name;
	}

	public function __destruct(){
		sqlsrv_close($this->conn);
	}
}

?>